var express = require('express');
var router = express.Router();
var personData = require('./person-data');

router.post('/get', (req, res)=>{
    let personid = req.body.personid;
    personData.get(personid, (err, results, fields)=>{
        let response = {};
        if(err){
            response.msg = ""+err;
            response.status = 'fail';
            res.send(response);
        } else{
            response.data =  results;
            response.msg = 'get person success.';
            response.status = 'success';
            res.send(response);
        }
        res.end(); 
    });
});
router.post('/add', (req, res)=>{
    let citizenid = req.body.citizenid;
    let firstname = req.body.firstname;
    let lastname = req.body.lastname;
    let birthdate = req.body.birthdate;
    
    if(!citizenid){
        let response = {
            status: "fail",
            msg: "Field 'citizenid' Is not Validate."
        };
        res.send(response);
        res.end(); 
        console.log('END');
    }else{
        let values = {
        "citizenid": citizenid, 
        "firstname": firstname,
        "lastname": lastname,
        "birthdate": birthdate
        };
        personData.add(values, (err, results, fields)=>{
            let response = {};
            if(err){
                response.msg = ''+err;
                response.status = 'fail'; 
            }else{
                response.msg = 'Added Person Success.';
                response.status = 'success';
                response.data = {personid: result.insertId};
            }
            res.send(response);
            res.end();
        });
    }
});
router.post('/update', (req, res)=>{


    
});

module.exports = router;