var connection = require('../../dbconnection');
var personData = {
    get: (personid, callback)=>{
        let sql = 'SELECT * FROM person WHERE personid = ?';
        connection.query(sql,[personid],(err, persons, fields)=>{
            if(err){
                console.log('ERROR',  err); 
            } else{
                console.log('SUCCESS',  persons); 
            }
            callback(err, persons, fields);
        }); 
    },
    search:(fullname, callback)=>{
        let sql = 'SELECT * FROM person WHERE firstname LIKE ? OR lastname LIKE ?';
        let params = ['%'+fullname+'%', '%'+fullname+'%'];
        connection.query(sql,params,(err, result, fields)=>{
            if(err){
                console.error('ERROR',  err); 
            } else{
                console.log('SUCCESS',  result); 
            }
            callback(err, result, fields);
        });
    },
    add: (values,callback)=>{
        let sql = 'INSERT INTO person SET ?';
        connection.query(sql, values, (err, result, fields)=>{
            if(err){
                console.log('ERROR',  err); 
            } else{
                console.log('SUCCESS',  result); 
            }
            callback(err, result, fields);
        });
    },
    update: (values, personid, callback)=>{
        let sql = 'UPDATE person SET ? WHERE personid=?';
        connection.query(sql, [values, personid], (err, result, fields)=>{
            if(err){
                console.log('ERROR',  err); 
            } else{
                console.log('SUCCESS',  result); 
            }
            callback(err, result, fields);
        }); 
    },
    delete: (personid, callback)=>{
        let sql = 'DELETE FROM person WHERE personid=?';
        connection.query(sql, [personid], (err, result, fields)=>{
            if(err){
                console.log('ERROR',  err); 
            } else{
                console.log('SUCCESS',  result); 
            }
            callback(err, result, fields);
        });
    }
} 

module.exports = personData;