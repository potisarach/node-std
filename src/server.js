var express = require('express');
var server = express();
var bodyParser = require('body-parser');
var personApi = require('./api/person/person-api');

server.use(bodyParser.json()); // support json encoded bodies
server.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

server.use('/api/person', personApi);

server.listen(8000, ()=>{
    console.log("listen", "http://localhost:8000");
});
